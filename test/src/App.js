import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PicoPlacaForm from './componentes/PicoPlacaForm.js';
import {querys} from './data/PicoPlaca.json'

class App extends Component{
  constructor(){
    super();
    this.state = {
      querys
    }
    this.handleAddPicoPlaca = this.handleAddPicoPlaca.bind(this);
  }
  removeTodo(index) {
    if (window.confirm("Are you sure?")){
    this.setState({
      querys: this.state.querys.filter((e, i) => {
        return i !== index
      })
    });
    }
  }
  handleAddPicoPlaca(query) {
    this.setState({
      querys: [...this.state.querys, query]
    })
  }
  render() {
    const querys = this.state.querys.map((query, i) => {
        return (
          <div className="col-md-4" key={i}>
            <div className="card mt-4">
              <div className="card-title text-center">
                <h3>Placa { i +1} : {query.placa}</h3>
                <span className="badge badge-pill badge badge-warning ml-2">
                  {query.done}
                </span>
              </div>
              <div className="card-body">
                {query.dateString}
              </div>
              <div className="card-footer">
                <button
                  className="btn btn-danger"
                  onClick={this.removeTodo.bind(this, i)}>
                  Delete
                </button>
              </div>
            </div>
          </div>
        )
      });
  return (
    <div className="App">
      <nav className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="/">
            Number of placas registered: 
            <span className="badge badge-pill badge-light ml-2">
              {this.state.querys.length}
            </span>
          </a>
        </nav>

      <header className="App-header">
        <h1 className="display-1" >Test Pico y Placa </h1>
          <div className="container">
            <div className="row mt-4">
              <div className="col-md-4 text-center">
                  <img src={logo} className="App-logo" alt="logo" />
              <PicoPlacaForm onAddTodo={this.handleAddPicoPlaca}></PicoPlacaForm>
              </div>

              <div className="col-md-8">
                <div className="row">
                  {querys}
                </div>
              </div>
            </div>
          </div>
       </header>
    </div>
  );
}
}
export default App;
