import React, {Component} from 'react';

class PicoPlacaForm extends Component {
    constructor () {
        super();
        this.state = {
          title: '',
          placa: '',
          dateString: '',
          hour: '',
          Authorized: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    
      handleSubmit(e) {
        e.preventDefault();
        var array = this.state.dateString.split('/');
        var numberPlaca= this.state.placa.substr(this.state.placa.length - 1,1);
        switch (new Date(array[array.length -2]+' '+array[array.length -1]+', '+array[array.length -3]+' 12:00:00').getUTCDay()) {
            case 0:
              this.state.done ="Authorized";
              break;
            case 1:
              if(numberPlaca > 0 && numberPlaca < 3)
                this.state.done ="Not Authorized";
              else
                this.state.done ="Authorized";
              break;
            case 2:
                if(numberPlaca > 2 && numberPlaca < 5)
                this.state.done ="Not Authorized";
              else
                this.state.done ="Authorized";
              break;
            case 3:
                if(numberPlaca > 4 && numberPlaca < 7)
                this.state.done ="Not Authorized";
              else
                this.state.done ="Authorized";
              break;
            case 4:
                if(numberPlaca > 6 && numberPlaca < 9)
                this.state.done ="Not Authorized";
              else
                this.state.done ="Authorized";
              break;
            case 5:
                if(numberPlaca == "0" || numberPlaca  == "9")
                this.state.done ="Not Authorized";
              else
                this.state.done ="Authorized";
              break;
            case 6:
            this.state.done ="Authorized";
          } 
        console.log(this.state.done);
        this.props.onAddTodo(this.state);
        this.setState({
          title: '',
          placa:'',
          dateString: '',
          hour: '',
          Authorized: ''
        });
      }
    
      handleInputChange(e) {
        const {value, name} = e.target;
        console.log(value, name);
        this.setState({
          [name]: value
        });
      }
    
      render() {
        return (
          <div className="card">
            <form onSubmit={this.handleSubmit} className="card-body">
              <div className="form-group">
                <input
                  type="text"
                  name="placa"
                  className="form-control"
                  value={this.state.placa}
                  onChange={this.handleInputChange}
                  pattern="[A-Z]{3}[0-9]{4}"
                  required= {true}
                  placeholder="PLACA EXAMPLE: PBC9578"
                  />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="dateString"
                  className="form-control"
                  value={this.state.dateString}
                  onChange={this.handleInputChange}
                  pattern="[0-9]{4}[/][0-1]{1}[0-9]{1}[/][0-9]{2}"
                  required= {true}
                  placeholder="Date Example: YYYY/MM/DD"
                  />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  name="hour"
                  className="form-control"
                  required= {true}
                  value={this.state.hour}
                  onChange={this.handleInputChange}
                  pattern="[0-1]{1}[0-9]{1}[:][0-5]{1}[0-9]{1}"
                  placeholder="Hour Example 18:56"
                  />
              </div>
              <button type="submit" className="btn btn-primary">
                Save
              </button>
            </form>
          </div>
        )
      }
}
export default PicoPlacaForm;